# OpenML dataset: Digital-currency---Time-series

https://www.openml.org/d/43563

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
Howdy folks! 
I have prepared a starter dataset for time series practice. This is my 1st upload. Any questions/feedback are welcome. 
Content

The data was prepared using Alpha Vantage API
The data represents historical daily time series for a digital currency (BTC) traded on the Saudi market (SAR/Sudi Riyal)
Prices and volumes are quoted in both SAR  USD.
Data date range: 2018-05-11 to 30.01.2021

Task: Use the past to predict the future!

Check Tasks tab

Acknowledgements
Special thanks to all my instructors and friends at GA.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43563) of an [OpenML dataset](https://www.openml.org/d/43563). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43563/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43563/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43563/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

